from flask import render_template, url_for, request, session, redirect, jsonify, abort, make_response
import bcrypt
import os
from flask import Flask
from bson.objectid import ObjectId
from bson import json_util
import json
from datetime import datetime

app = Flask(__name__)

@app.route('/')
def index():
    data = {'TEST':"Hello world"}
    return jsonify(data)

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

def main():
    app.run(host='127.0.0.1', debug=False)

if __name__ == "__main__":
    main()